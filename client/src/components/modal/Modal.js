import Backdrop from "../backdrop/Backdrop";
import classes from "./Modal.module.css";

const Modal =(props) =>{
 return (
   <Backdrop onClose={props.onClose}>
     <div className={classes.modal}>
       <div className={classes.header_container}>
         <div>
           <span className={classes.header}>Betway</span>
           <span className={classes.close_btn} onClick={props.onClose}>
             &times;
           </span>
         </div>
       </div>
       <div className={classes.modal_body}>
          <span className={classes.body_text}>{props.message}</span>
       </div>
     </div>
   </Backdrop>
 );
}

export default Modal;