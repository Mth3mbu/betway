import { useState } from "react";
import axios from "axios";
import classes from "./Login.module.css";

const Login = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameTouched, setUsernameTouched] = useState(false);
  const [passwordTouched, setPasswordTouched] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [authErrorMessage, setAuthErrorMessage] = useState("");

  const usernameRgex =
    /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  const usernameIsValid =
    username.trim().length > 0 && usernameRgex.test(username);
  let passwordIsValid = password.trim() !== "" && password.trim().length >= 8;
  let usernameIsInvalid = !usernameIsValid && usernameTouched;
  const passwordIsInvalid = !passwordIsValid && passwordTouched;
  const emailErrorMessage =
    username.length === 0
      ? "Username must not be empty"
      : "Username must be a valid email address";
  const passwordErrorMessage =
    password.length === 0
      ? "Password must not be empty"
      : "Password must be at least 8 characters long";

  const usernameInputHandler = (event) => {
    setUsername(event.target.value);
  };

  const usernameBlurHandler = () => {
    setUsernameTouched(true);
  };

  const passwordInputHandler = (event) => {
    setPassword(event.target.value);
  };

  const passwordBlurHandler = () => {
    setPasswordTouched(true);
  };

  const formSubmitHandler = (event) => {
    event.preventDefault();
    setIsLoading(true);
    setUsernameTouched(true);
    setPasswordTouched(true);
    setAuthErrorMessage('');
  
    if (!usernameIsValid || !passwordIsValid) {
      setIsLoading(false);
      return;
    }

    axios
      .post("https://localhost:44356/auth/login", {
        username: username,
        password: password,
      })
      .then((response) => {
        setIsLoading(false);
        props.onLoginSuccess(response?.data?.message);
      })
      .catch((error) => {
        setAuthErrorMessage(error?.response?.data?.message);
        setIsLoading(false);
      });
  };

  return (
    <>
      <form onSubmit={formSubmitHandler}>
        <div className={classes.modal}>
          <div className={classes.header_container}>
            <div className={classes.login_header}>
              <span className={classes.header}>Login</span>
              <span className={classes.close_btn} onClick={props.onClose}>
                &times;
              </span>
            </div>
            <div>
              <span className={classes.customer}>New Customer?</span>
              <span className={`${classes.register} ${classes.customer}`}>
                Register here
              </span>
            </div>
          </div>
          <div className={classes.input_container}>
            <div className={classes.form_group}>
              <label>Username</label>
              <input
                className={`${classes.input} ${
                  usernameIsInvalid
                    ? classes.invalid_input
                    : classes.valid_input
                }`}
                type="text"
                placeholder="Username"
                onChange={usernameInputHandler}
                onBlur={usernameBlurHandler}
                value={username}
              />
              {usernameIsInvalid && (
                <span className={classes.error_msg}>{emailErrorMessage}</span>
              )}
            </div>
            <div className={classes.form_group}>
              <label>Password</label>
              <input
                className={`${classes.input} ${
                  passwordIsInvalid
                    ? classes.invalid_input
                    : classes.valid_input
                }`}
                type="password"
                placeholder="Password"
                onChange={passwordInputHandler}
                onBlur={passwordBlurHandler}
                value={password}
              />
              {passwordIsInvalid && (
                <span className={classes.error_msg}>
                  {passwordErrorMessage}
                </span>
              )}
            </div>
          </div>
          <div className={classes.actions_container}>
            <button className={classes.login_btn} type="submit">
              Login
            </button>
            <span className={classes.forgot_password}>
              Forgot Username/Password
            </span>
           {authErrorMessage && <span className={classes.auth_error}>{authErrorMessage}</span>}
          </div>
          {isLoading && (
            <div className={classes.loader_container}>
              <div className={classes.loader}></div>
            </div>
          )}
        </div>
      </form>
    </>
  );
};

export default Login;
