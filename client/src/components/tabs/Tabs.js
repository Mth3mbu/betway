import React, { useRef } from "react";
import "./Tabs.css";

const Tabs = (props) => {
  const tabHeader = useRef();
  const tabIndicator = useRef();
  const tabBody = useRef();
  const tabsMap = props.tabOptions;
  const tabs = Object.keys(tabsMap);

  const handleTabClick = (e) => {
    const tabsPane = tabHeader.current.children;
    for (let i = 0; i < tabsPane.length; i++) {
      tabsPane[i].classList.remove("active");
    }
    const key = e.target.innerText.toLowerCase();
    e.target.classList.add("active");
    tabIndicator.current.style.left = `calc(calc(100% / 5) * ${tabsMap[key]})`;
  };

  return (
    <>
      <div className="tabs">
        <div className="tab-header" ref={tabHeader}>
          {tabs.map((tab, index) => (
            <div
              onClick={handleTabClick}
              className={index === 0 ? "active" : ""}
              key={tab}
            >
              {tab}
            </div>
          ))}
        </div>
        <div className="tab-indicator" ref={tabIndicator}></div>
        <div className="tab-body" ref={tabBody}>
        </div>
      </div>
    </>
  );
};

export default Tabs;
