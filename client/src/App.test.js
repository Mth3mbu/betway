import { render, screen, fireEvent } from "@testing-library/react";
import App from "./App";
import Tabs from "./components/tabs/Tabs";

test("renders login button", () => {
  render(<App />);

  const loginButton = screen.getByText("login");

  expect(loginButton).toBeInTheDocument();
});

test("renders signup button", () => {
  render(<App />);

  const signUpButton = screen.getByText("Sign up");

  expect(signUpButton).toBeInTheDocument();
});

test("renders Join now button", () => {
  render(<App />);

  const joinNowButton = screen.getByText("Join Now");

  expect(joinNowButton).toBeInTheDocument();
});

test("renders login modal on login button click", () => {
  const { getByRole } = render(<App />);
  const loginButton = getByRole("button", { name: "login" });
  fireEvent.click(loginButton);

  const usernameTextbox = screen.getByText("Username");
  const passwordTextbox = screen.getByText("Password");
  const registerButton = screen.getByText("Register here");

  expect(usernameTextbox).toBeInTheDocument();
  expect(passwordTextbox).toBeInTheDocument();
  expect(registerButton).toBeInTheDocument();
});

test("renders five tabs", () => {
  const pageContent = {
    tabs: { Sports: 0, Live: 1, Casino: 2, Vegas: 3, eSports: 4 },
    logoImageUrl: "https://betway.com/img/betway-logo.svg",
    backgroundImageUrl: "https://betway.com/img/betway-logo.svg",
    offer: {
      name: "Betway Sports Welcome Offer",
      description: "100% Match Bonus up to £50 on 1st deposit of £10",
    },
  };

  render(<Tabs tabOptions={pageContent?.tabs} />);

  const sportsTab = screen.getByText("Sports");
  const liveTab = screen.getByText("Live");
  const casinoTab = screen.getByText("Casino");
  const vegasTab = screen.getByText("Vegas");
  const eSportsTab = screen.getByText("eSports");

  expect(sportsTab).toBeInTheDocument();
  expect(liveTab).toBeInTheDocument();
  expect(casinoTab).toBeInTheDocument();
  expect(vegasTab).toBeInTheDocument();
  expect(eSportsTab).toBeInTheDocument();
});
