import React, {useState, useEffect} from "react";
import axios from "axios";
import Tabs from "./components/tabs/Tabs";
import Login from "./components/login/Login";
import Modal from "./components/modal/Modal";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "./App.css";

export default function BasicTabs() {
  const [showLogin, setShowLogin] = useState(false);
  const [showWelcomeModal, setShowWelcomeModal] = useState(false);
  const [welcomeMessage, setWelcomeMessage] = useState('');
  const [pageContent, setPageContent] = useState({});

  useEffect(() => {
    axios.get('https://localhost:44356/page/content')
    .then((response) => {
      setPageContent(response.data);
    }).catch(() => {
      toast.error('Opps! something went wrong while trying to fetch page content',{
        position: toast.POSITION.TOP_LEFT,
      });
    })
  },
  []);

  const openLoginHandler = () => {
    setShowLogin(true);
  }

  const closeLoginHandler = () => {
    setShowLogin(false);
  }

  const loginSuccessHandler = (welcomeMessage) => {
    setShowLogin(false);
    setShowWelcomeModal(true);
    setWelcomeMessage(welcomeMessage);
  }

  const closeWelcomeModalHandler = () => {
    setShowWelcomeModal(false);
  }

  return (
    <div
      className="main-container"
      style={{
        backgroundImage:`url(${pageContent.backgroundImageUrl})`,
      }}
    >
      <div className="header-container">
        <img
          src={pageContent.logoImageUrl}
          height={25}
          alt="betway-logo"
        />
        <div>
          <button className="button login-button" onClick={openLoginHandler}>login</button>
          <button className="button register-button">Sign up</button>
        </div>
      </div>
       {pageContent?.tabs && <Tabs tabOptions={pageContent?.tabs}/> }
       {showLogin && <Login onClose={closeLoginHandler} onLoginSuccess={loginSuccessHandler}/>}
       {showWelcomeModal && <Modal onClose={closeWelcomeModalHandler } message={welcomeMessage}/> }
       <ToastContainer />
      <div className="footer">
        <div className="offer">{pageContent?.offer?.name}</div>
        <div className="figure">{pageContent?.offer?.description}</div>
        <button className="join-button">Join Now</button>
      </div>
    </div>
  );
}
