using Betway.Service.Extensions;
using Betway.Web.API.Infrastructure;
using Betway.Web.API.IocConfigs;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddPresenters()
                .AddServices()
                .AddRepositories()
                .AddFluentValidation();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

var corsSettings = builder.Configuration.Read<CorsSettings>("Cors");

app.UseCors(builder => builder
    .WithOrigins(corsSettings.Origins)
    .AllowAnyHeader()
    .AllowAnyMethod()
);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
