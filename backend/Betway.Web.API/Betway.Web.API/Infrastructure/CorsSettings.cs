﻿namespace Betway.Web.API.Infrastructure
{
    public class CorsSettings
    {
        public string[] Origins { get; set; }
    }
}
