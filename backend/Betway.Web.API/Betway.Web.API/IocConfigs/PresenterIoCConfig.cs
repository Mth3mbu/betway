﻿using Betway.Service.Interfaces.Output;
using Betway.Service.Output;

namespace Betway.Web.API.IocConfigs
{
    public static class PresenterIoCConfig
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            services.AddScoped(typeof(ISuccessOrErrorActionResultPresenter<,>), typeof(SuccessOrErrorRestPresenter<,>));

            return services;
        }
    }
}
