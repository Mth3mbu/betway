﻿using Betway.Service.Models;
using Betway.Service.Validation;
using FluentValidation;
using FluentValidation.AspNetCore;

namespace Betway.Web.API.IocConfigs
{
    public static class FluentValidatorIoCConfig
    {
        public static IServiceCollection AddFluentValidation(this IServiceCollection services)
        {
            services.AddFluentValidationAutoValidation()
                .AddFluentValidationClientsideAdapters();
            services.AddScoped<IValidator<LoginCredentials>, LoginCredentialsValidator>();

            return services;
        }
    }
}
