﻿using Betway.Service;
using Betway.Service.Interfaces.services;

namespace Betway.Web.API.IocConfigs
{
    public static class ServicesIoCConfig
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IContentService, ContentService>();

            return services;
        }
    }
}
