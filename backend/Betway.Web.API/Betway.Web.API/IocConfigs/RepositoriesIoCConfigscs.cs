﻿using Betway.Repositories.Interfaces;
using Betway.Repositories;

namespace Betway.Web.API.IocConfigs
{
    public static class RepositoriesIoCConfigscs
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IContentRepository, ContentRepository>();

            return services;
        }
    }
}
