﻿using Betway.Repositories.Models;
using Betway.Service.Interfaces.Output;
using Betway.Service.Interfaces.services;
using Betway.Service.Models;
using Microsoft.AspNetCore.Mvc;

namespace Betway.Web.API.Controllers
{
    [ApiController]
    [Route("page/content")]
    public class ContentController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<PageContent, ErrorDto> _presenter;
        private readonly IContentService _contentService;
        public ContentController(ISuccessOrErrorActionResultPresenter<PageContent, ErrorDto> presenter, IContentService contentService)
        {
            _presenter = presenter;
            _contentService = contentService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPageContent()
        {
            await _contentService.GetPageContent(_presenter);

            return _presenter.Render();
        }
    }
}
