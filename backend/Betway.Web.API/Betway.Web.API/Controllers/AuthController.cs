﻿using Betway.Service.Interfaces.Output;
using Betway.Service.Interfaces.services;
using Betway.Service.Models;
using Microsoft.AspNetCore.Mvc;

namespace Betway.Web.API.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthController : ControllerBase
    {
        private readonly ISuccessOrErrorActionResultPresenter<AuthResponse, ErrorDto> _presenter;
        private readonly ILoginService _loginService;

        public AuthController(ISuccessOrErrorActionResultPresenter<AuthResponse, ErrorDto> presenter, ILoginService loginService)
        {
            _presenter = presenter;
            _loginService = loginService;
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login(LoginCredentials credentials)
        {
            await _loginService.Login(_presenter, credentials);

            return _presenter.Render();
        }
    }
}
