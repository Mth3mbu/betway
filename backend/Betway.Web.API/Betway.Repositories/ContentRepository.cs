﻿using Betway.Repositories.Interfaces;
using Betway.Repositories.Models;
using Microsoft.Extensions.Configuration;

namespace Betway.Repositories
{
    public class ContentRepository : IContentRepository
    {
        private readonly IConfiguration _configuration;
        public ContentRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<PageContent> GetContent()
        {
            await Task.Delay(1000);
      
            var tabs = _configuration.GetSection("PageContent:Tabs").GetChildren().Select(x => x.Value).ToArray();
            var tabsDictionary = new Dictionary<string, int>();

            for (int i = 0; i < tabs.Length; i++)
            {
                tabsDictionary[tabs[i]] = i;
            }

            return new PageContent
            {
                BackgroundImageUrl = _configuration.GetSection("PageContent:BackgroundImageUrl").Value,
                LogoImageUrl = _configuration.GetSection("PageContent:LogoImageUrl").Value,
                Tabs = tabsDictionary,
                Offer = new CustomerOffer
                {
                    Description = _configuration.GetSection("PageContent:Offer:Description").Value,
                    Name = _configuration.GetSection("PageContent:Offer:Name").Value,
                }
            };
        }
    }
}
