﻿using Betway.Repositories.Interfaces;
using Betway.Repositories.Models;
using Microsoft.Extensions.Configuration;

namespace Betway.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _configuration;
        public UserRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<User> GetByUserByUsernameAndPassword(LoginDetails loginDetails)
        {
            await Task.Delay(1000);

            var user = new User
            {
                Username = _configuration.GetSection("User:Username").Value,
                Password = _configuration.GetSection("User:Password").Value,
                Fullname = _configuration.GetSection("User:Fullname").Value
            };

            return user?.Username == loginDetails?.Username && user?.Password == loginDetails?.Password ? user : null;
        }
    }
}
