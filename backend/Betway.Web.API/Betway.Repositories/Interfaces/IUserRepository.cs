﻿using Betway.Repositories.Models;

namespace Betway.Repositories.Interfaces
{
    public interface IUserRepository
    {
      Task<User> GetByUserByUsernameAndPassword(LoginDetails loginDetails);
    }
}
