﻿using Betway.Repositories.Models;

namespace Betway.Repositories.Interfaces
{
    public interface IContentRepository
    {
        Task<PageContent> GetContent();
    }
}
