﻿namespace Betway.Repositories.Models
{
    public class PageContent
    {
        public string BackgroundImageUrl { get; set; }
        public string LogoImageUrl { get; set; }
        public Dictionary<string, int> Tabs { get; set; }
        public CustomerOffer Offer { get; set; }
    }
}
