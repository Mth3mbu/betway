﻿
namespace Betway.Repositories.Models
{
    public class CustomerOffer
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
