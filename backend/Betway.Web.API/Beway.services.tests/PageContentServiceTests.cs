﻿using Betway.Repositories.Interfaces;
using Betway.Repositories.Models;
using Betway.Service;
using Betway.Service.Models;
using Betway.Service.Output;
using Beway.services.tests.Builders;
using NSubstitute;
using NUnit.Framework;

namespace Beway.services.tests
{
    [TestFixture]
    internal class PageContentServiceTests
    {
        [Test]
        public async Task GetPageContent_Should_return_Content()
        {
            //Arrange
            var offer = CustomerOfferBuilder.Create()
                .WithName("Test offer")
                .WithDescription("5$ offer")
                .Build();
            var pageContent = PageContentBuilder.Create()
                .WithTabs(new string[] { "tab1", "tab2", "tab3" })
                .WithOffer(offer)
                .WithLogoImageUrl("https:logo.com")
                .WithBackgroundImageUrl("https:background.com").Build();
            var contentRepository = Substitute.For<IContentRepository>();
            contentRepository.GetContent().Returns(pageContent);
            var presenter = new SuccessOrErrorRestPresenter<PageContent, ErrorDto>();

            //Act
            var sut = new ContentService(contentRepository);
            await sut.GetPageContent(presenter);
            var results = presenter.GetSuccessResponse();
            var errorResults = presenter.GetErrorResponse();

            //Assert
            Assert.Null(errorResults);
            Assert.AreEqual(pageContent.Tabs.Count(), results.Tabs.Count());
            Assert.AreEqual(pageContent.LogoImageUrl, results.LogoImageUrl);
            Assert.AreEqual(pageContent.BackgroundImageUrl, results.BackgroundImageUrl);
            Assert.AreEqual(pageContent.Offer.Name, results.Offer.Name);
            Assert.AreEqual(pageContent.Offer.Description, results.Offer.Description);
        }
    }
}
