﻿using Betway.Repositories.Interfaces;
using Betway.Repositories.Models;
using Betway.Service;
using Betway.Service.Models;
using Betway.Service.Output;
using Beway.services.tests.Builders;
using NSubstitute;
using NUnit.Framework;

namespace Beway.services.tests
{
    [TestFixture]
    internal class LoginServiceTests
    {
        [Test]
        public async Task Given_Correct_Credentials_Should_Return_Welcome_Message()
        {
            //Arrange
            var userName = "jack";
            var password = "password";
            var loginCredentials = LoginCredentialBuilder.Create()
                .WithUsername(userName)
                .WithPassword(password)
                .Build();
            var userRepository = Substitute.For<IUserRepository>();
            var presenter = new SuccessOrErrorRestPresenter<AuthResponse, ErrorDto>();
            var user = UserBuilder.Create()
                .WithFullName("Jack mthembu")
                .WithUserName(userName)
                .WithPassword(password)
                .Build();
            userRepository.GetByUserByUsernameAndPassword(Arg.Any<LoginDetails>()).Returns(user);
            var expectedResults = $"Welcome to Betway {user.Fullname}!";

            //Act
            var sut = new LoginService(userRepository);
            await sut.Login(presenter, loginCredentials);
            var successResponse = presenter.GetSuccessResponse();
            var errorResponse = presenter.GetErrorResponse();

            //Assert
            Assert.AreEqual(expectedResults, successResponse.Message);
            Assert.IsNull(errorResponse);
        }

        [Test]
        public async Task Given_InCorrect_Credentials_Should_Return_Error_Message()
        {
            //Arrange
            var userName = "jack";
            var password = "password";
            var expectedResults = "Invalid Login credentials";
            var loginCredentials = LoginCredentialBuilder.Create()
                .WithUsername(userName)
                .WithPassword(password)
                .Build();
            var userRepository = Substitute.For<IUserRepository>();
            var presenter = new SuccessOrErrorRestPresenter<AuthResponse, ErrorDto>();

            //Act
            var sut = new LoginService(userRepository);
            await sut.Login(presenter, loginCredentials);
            var successResponse = presenter.GetSuccessResponse();
            var errorResponse = presenter.GetErrorResponse();

            //Assert
            Assert.AreEqual(expectedResults, errorResponse.Message);
            Assert.IsNull(successResponse);
        }
    }
}
