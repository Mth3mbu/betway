﻿using Betway.Repositories.Models;

namespace Beway.services.tests.Builders
{
    internal class LoginDetailsBuilder
    {
        private LoginDetails loginDetails = new LoginDetails();

        public static LoginDetailsBuilder Create()
        {
            return new LoginDetailsBuilder();
        }

        public LoginDetailsBuilder WithUserName(string value)
        {
            loginDetails.Username = value;
            return this;
        }

        public LoginDetailsBuilder WithPassword(string value)
        {
            loginDetails.Password = value;
            return this;
        }

        public LoginDetails Build()
        {
            return loginDetails;
        }
    }
}
