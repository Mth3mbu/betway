﻿using Betway.Service.Models;

namespace Beway.services.tests.Builders
{
    public class LoginCredentialBuilder
    {
        private static LoginCredentials loginCredentials = new LoginCredentials();

        public LoginCredentialBuilder WithUsername(string username)
        {
            loginCredentials.Username = username;

            return this;
        }

        public LoginCredentialBuilder WithPassword(string password)
        {
            loginCredentials.Password = password;
            return this;

        }

        public static LoginCredentialBuilder Create()
        {
            return new LoginCredentialBuilder();
;        }

        public  LoginCredentials Build()
        {
            return loginCredentials;
        }
    }
}
