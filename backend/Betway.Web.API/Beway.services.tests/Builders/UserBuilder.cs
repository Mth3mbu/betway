﻿using Betway.Repositories.Models;

namespace Beway.services.tests.Builders
{
    internal class UserBuilder
    {
        private User user = new User();
        public UserBuilder WithFullName(string value)
        {
            user.Fullname = value;
            return this;
        }

        public UserBuilder WithUserName(string value)
        {
            user.Username = value;
            return this;
        }

        public UserBuilder WithPassword(string value)
        {
            user.Password = value;
            return this;
        }

        public static UserBuilder Create()
        {
            return new UserBuilder();
        }

        public User Build()
        {
            return user;
        }
    }
}
