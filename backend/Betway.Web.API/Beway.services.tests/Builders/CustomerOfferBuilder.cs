﻿using Betway.Repositories.Models;

namespace Beway.services.tests.Builders
{
    internal class CustomerOfferBuilder
    {
        private CustomerOffer customerOffer = new CustomerOffer();

        public CustomerOfferBuilder WithDescription(string value)
        {
            customerOffer.Description = value;
            return this;
        }

        public CustomerOfferBuilder WithName(string value)
        {
            customerOffer.Name = value;
            return this;
        }

        public static CustomerOfferBuilder Create()
        {
            return new CustomerOfferBuilder();
        }

        public CustomerOffer Build()
        {
            return customerOffer;
        }
    }
}
