﻿using Betway.Repositories.Models;

namespace Beway.services.tests.Builders
{
    internal class PageContentBuilder
    {
        private PageContent pageContent = new PageContent();

        public PageContentBuilder WithLogoImageUrl(string value)
        {
            pageContent.LogoImageUrl = value;

            return this;
        }

        public PageContentBuilder WithBackgroundImageUrl(string value)
        {
            pageContent.BackgroundImageUrl = value;

            return this;
        }

        public PageContentBuilder WithTabs(string[] value)
        {
            var tabsDictionary = new Dictionary<string, int>();
            for (int i = 0; i < value.Length; i++)
            {
                tabsDictionary[value[i]] = i;
            }
            pageContent.Tabs = tabsDictionary;

            return this;
        }

        public PageContentBuilder WithOffer(CustomerOffer value)
        {
            pageContent.Offer = value;

            return this;
        }

        public static PageContentBuilder Create()
        {
            return new PageContentBuilder();
        }

        public PageContent Build()
        {
            return pageContent;
        }
    }
}
