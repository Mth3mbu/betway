﻿namespace Betway.Service.Output
{
    public class PresenterException : Exception
    {
        public PresenterException(string message) : base(message)
        {

        }
    }
}
