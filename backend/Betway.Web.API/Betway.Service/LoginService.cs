﻿using Betway.Repositories.Interfaces;
using Betway.Service.Interfaces.Output;
using Betway.Service.Interfaces.services;
using Betway.Service.Models;

namespace Betway.Service
{
    public class LoginService : ILoginService
    {
        private readonly IUserRepository _userRepository;
        public LoginService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task Login(ISuccessOrErrorPresenter<AuthResponse, ErrorDto> presenter, LoginCredentials credentials)
        {
            var user = await _userRepository.GetByUserByUsernameAndPassword(new Repositories.Models.LoginDetails
            {
                Password = credentials.Password,
                Username = credentials.Username
            });

            if (user != null)
                presenter.Success(new AuthResponse { Message = $"Welcome to Betway {user.Fullname}!" });
            else
                presenter.Error(new ErrorDto { Message = "Invalid Login credentials" });

        }
    }
}
