﻿using Betway.Repositories.Interfaces;
using Betway.Repositories.Models;
using Betway.Service.Interfaces.Output;
using Betway.Service.Interfaces.services;
using Betway.Service.Models;

namespace Betway.Service
{
    public class ContentService : IContentService
    {
        private readonly IContentRepository _contentRepository;
        public ContentService(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public async Task GetPageContent(ISuccessOrErrorPresenter<PageContent, ErrorDto> presenter)
        {
            var content = await _contentRepository.GetContent();

            presenter.Success(content);
        }
    }
}
