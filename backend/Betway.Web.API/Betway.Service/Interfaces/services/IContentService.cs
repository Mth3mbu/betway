﻿using Betway.Repositories.Models;
using Betway.Service.Interfaces.Output;
using Betway.Service.Models;

namespace Betway.Service.Interfaces.services
{
    public interface IContentService
    {
        Task GetPageContent(ISuccessOrErrorPresenter<PageContent, ErrorDto> presenter);
    }
}
