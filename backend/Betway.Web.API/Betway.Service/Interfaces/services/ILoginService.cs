﻿using Betway.Service.Interfaces.Output;
using Betway.Service.Models;

namespace Betway.Service.Interfaces.services
{
    public interface ILoginService
    {
        Task Login(ISuccessOrErrorPresenter<AuthResponse, ErrorDto> presenter, LoginCredentials credentials);
    }
}
