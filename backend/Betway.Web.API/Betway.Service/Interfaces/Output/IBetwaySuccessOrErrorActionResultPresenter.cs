﻿using Microsoft.AspNetCore.Mvc;

namespace Betway.Service.Interfaces.Output
{
    public interface IBetwaySuccessOrErrorActionResultPresenter<TSuccess, TError> : ISuccessOrErrorPresenter<TSuccess, TError>
    {
        public IActionResult Render();
    }
}
