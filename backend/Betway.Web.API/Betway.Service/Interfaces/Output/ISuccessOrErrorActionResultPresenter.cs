﻿using Microsoft.AspNetCore.Mvc;

namespace Betway.Service.Interfaces.Output
{
    public interface ISuccessOrErrorActionResultPresenter<TSuccess, TError> : ISuccessOrErrorPresenter<TSuccess, TError>
    {
        public IActionResult Render();
    }
}
