﻿namespace Betway.Service.Interfaces.Output
{
    public interface ISuccessOrErrorPresenter<TSuccess, TError> : IErrorPresenter<TError>
    {
        public void Success(TSuccess successResult);
        public TSuccess GetSuccessResponse();
        public TError GetErrorResponse();
    }
}
